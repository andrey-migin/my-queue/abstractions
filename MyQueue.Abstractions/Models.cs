using System;
using System.Runtime.Serialization;

namespace MyQueue.Abstractions
{
    [DataContract]
    public class MyQueueMessage
    {
        [DataMember(Order = 1)]
        public long Id { get; set; }
        
        [DataMember(Order = 2)]
        public int AttemptNo { get; set; }
        
        [DataMember(Order = 3)]
        public byte[] Content { get; set; }
    }


    public static class MessageGrpcModelExtension
    {
        public static DateTimeOffset GetDateTime(this MyQueueMessage message)
        {
            return DateTimeOffset.FromUnixTimeMilliseconds(message.Id);
        }
    }
}
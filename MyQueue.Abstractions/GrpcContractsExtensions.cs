using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyQueue.Abstractions
{
    public static class GrpcContractsExtensions
    {

        private static CommitMessageGrpcContract ToCommitContract(this MyQueueMessage src, string queueId)
        {
            return new ()
            {
                QueueId = queueId,
                IdsToCommit = new []{src.Id}
            };
        }
        
        private static IEnumerable<CommitMessageGrpcContract> ToCommitContracts(this IEnumerable<MyQueueMessage> messages,
            string queueId,
            int maxMessagesPerContract)
        {

            var result = new List<long>();

            foreach (var message in messages)
            {
                result.Add(message.Id);

                if (result.Count < maxMessagesPerContract) 
                    continue;
                yield return new CommitMessageGrpcContract
                {
                    QueueId = queueId,
                    IdsToCommit = result.ToArray()
                };
                    
                result.Clear();
            }

            if (result.Count > 0)
            {
                yield return new CommitMessageGrpcContract
                {
                    QueueId = queueId,
                    IdsToCommit = result.ToArray()
                };  
            }

        }

        public static ValueTask CommitMessageAsync(this IMyQueueGrpcService grpcService, string queueId, MyQueueMessage message)
        {
            return grpcService.CommitMessagesAsync(message.ToCommitContract(queueId));
        }
        
        public static async ValueTask CommitMessagesAsync(this IMyQueueGrpcService grpcService, 
            string queueId, IEnumerable<MyQueueMessage> messages, int maxMessagesPerRequest)
        {
            foreach (var grpcContract in messages.ToCommitContracts(queueId, maxMessagesPerRequest))
                await grpcService.CommitMessagesAsync(grpcContract);
        }
        
    }
}
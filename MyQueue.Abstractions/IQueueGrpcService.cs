﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Grpc.Core;

namespace MyQueue.Abstractions
{
    
    [ServiceContract(Name = "MyQueue")]
    public interface IMyQueueGrpcService
    {
        [OperationContract(Action = "CreateQueue")]
        ValueTask CreateQueueAsync(CreateQueueGrpcContract request);
        
        [OperationContract(Action = "Enqueue")]
        ValueTask EnqueueAsync(IAsyncEnumerable<EnqueueGrpcContract> messages, ServerCallContext context = null);

        [OperationContract(Action = "Dequeue")]
        IAsyncEnumerable<MyQueueMessage> DequeueAsync(DequeueGrpcContract contract, ServerCallContext context = null);

        [OperationContract(Action = "Commit")]
        ValueTask CommitMessagesAsync(CommitMessageGrpcContract request, ServerCallContext context = null);
    }
}
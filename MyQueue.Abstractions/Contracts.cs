using System.Runtime.Serialization;

namespace MyQueue.Abstractions
{
    [DataContract]
    public class CreateQueueGrpcContract
    {
        [DataMember(Order = 1)]
        public string AppName { get; set; }
        
        [DataMember(Order = 2)]
        public string QueueId { get; set; }
    }
    
    [DataContract]
    public class EnqueueGrpcContract
    {
        [DataMember(Order = 1)]
        public string QueueId { get; set; }
        
        [DataMember(Order = 2)]
        public byte[] Content { get; set; }
    }


    [DataContract]
    public class DequeueGrpcContract
    {
        [DataMember(Order = 1)]
        public string AppName { get; set; }
        
        [DataMember(Order = 2)]
        public string QueueId { get; set; }
        
        [DataMember(Order = 3)]
        public int MaxAmount { get; set; }
        
        [DataMember(Order = 4)]
        public int CommitSecondsDelay { get; set; }
    }

    [DataContract]
    public class CommitMessageGrpcContract
    {      
        [DataMember(Order = 1)]
        public string AppName { get; set; }
        
        [DataMember(Order = 2)]
        public string QueueId { get; set; }
        
        [DataMember(Order = 3)]
        public long[] IdsToCommit { get; set; }
    }
}